# COPYRIGHT (C) 2016 By Park, Sihyung
# module for preprocessing - transform custom data into ISRIP input format data

from utils import Processed, RNASeq, MicroArray

class ExpTagCalculator():
    """
    Fit and transform loaded data format into ISRIP input format.
    """
    def __init__(self, data=None, dtype=None):
        """
        * Input
         - data: loaded data in DataFrame object.
               If you want to use your custom data, it should be loaded before by
               `load_custom_array` or `load_custom_rnaseq` in `isrip.datasets`.
         - dtype: 'rnaseq' or 'array'.
        """
        self.data = data
        self.dtype = dtype

    def fit(self, data, dtype):
        """
        * Input
         - data: loaded data in DataFrame object.
               If you want to use your custom data, it should be loaded before by
               `load_custom_array` or `load_custom_rnaseq` in `isrip.datasets`.
         - dtype: 'rnaseq' or 'array'.
        """
        self.data = data
        if (not isinstance(self.data, RNASeq)) and (not isinstance(self.data, MicroArray)):     # pragma: no cover
            raise TypeError("inappropriate type: `{}`. expected `{}` or `{}`".format(type(self.data), type(RNASeq()), type(MicroArray())))
        self.dtype = dtype
        if self.dtype.lower() not in ["rnaseq", "array"]:       # pragma: no cover
            raise ValueError("dtype should be 'rnaseq' or 'array', not '{}''".format(self.dtype))

    def transform(self, total_tags, frag_avg_size=50.):
        if self.dtype == "rnaseq":
            res = self.data.copy()
            res.dropna(inplace=True)
            res.reset_index(drop=True, inplace=True)
            res["RPM"] = res.iloc[:, 1] * res.iloc[:, 2] / 1000.
            res["exp_tag"] = (res.RPM / res.RPM.sum() * total_tags).round().astype(int)
            #res["exp_tag"] = (res.iloc[:, 1] * res.iloc[:, 2] / 1000. / 1000000 * total_tags).astype(int)
            del res["RPM"]
            return Processed(res)
        elif self.dtype == "array":
            res = self.data.copy()
            res.dropna(inplace=True)
            res.reset_index(drop=True, inplace=True)
            res["num_fragment"] = res.iloc[:, 2] * res.iloc[:, 1] / frag_avg_size
            res["sum_fragment"] = res.iloc[:, 3].sum()
            res["exp_tag"] = (res.iloc[:, 3] / res.iloc[:, 4] * total_tags).round().astype(int)
            del res["num_fragment"]
            del res["sum_fragment"]
            return Processed(res)
        else:       # pragma: no cover
            raise ValueError("not a valid dtype: {}".format(self.dtype))

    def fit_transform(self, data, dtype, total_tags, frag_avg_size=50.):
        self.fit(data, dtype)
        return self.transform(total_tags, frag_avg_size)
