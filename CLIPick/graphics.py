# COPYRIGHT (C) 2016 By Park, Sihyung
# module for graphical representation

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sqlite3 as db
import sys
from collections import OrderedDict
from tqdm import tqdm
from peak_calling import smoothing_bedgraph
from __init__ import get_temp_data
from utils import SmoothBedGraph, CalledPeak, ReportedPeak, NoClusterError

mpl.rcParams['xtick.direction'] = 'out'
mpl.rcParams['ytick.direction'] = 'out'

class BedGraphVisualizer(object):

    def __init__(self, smooth_bedgraph, reported_peak=False, bedgraph_from_file=False, peak_from_file=False):
        self.bedgraph = smooth_bedgraph
        if bedgraph_from_file:
            self.bedgraph = SmoothBedGraph.load(self.bedgraph)
        if not isinstance(reported_peak, bool):       # pragma: no cover
            self.peak = reported_peak
            if not peak_from_file:
                if not isinstance(self.peak, ReportedPeak):       # pragma: no cover
                    raise TypeError("inappropriate type: {}. expected {}".format(type(self.peak), type(ReportedPeak())))
            if peak_from_file:
                self.peak = ReportedPeak.load(self.peak)
                del self.peak["length"]
                del self.peak["expression"]
                del self.peak["exp_tag"]
                del self.peak["threshold"]
                del self.peak["thres_cum_prob"]
                del self.peak["FDR_<="]
                #self.peak.columns = ["RefSeq", "peak", "peak_height", "chrom", "peak_pos", "strand"]
        if not isinstance(self.bedgraph, SmoothBedGraph):       # pragma: no cover
            raise TypeError("inappropriate type: {}. expected {}".format(type(self.bedgraph), type(SmoothBedGraph())))

    def draw(self, peak=False, xstart=None, xend=None, ystart=None, yend=None, markerfmt="ro", linefmt="r:"):
        chromosomes = self.bedgraph.ix[:, 0].unique()
        chromosomes = chromosomes[chromosomes!="nan"]
        fig = plt.figure(figsize=(12,5*len(chromosomes)))

        for i_th, chromosome in tqdm(enumerate(chromosomes), desc="BEDGRAPH"):
            bedgraph_chr = self.bedgraph[self.bedgraph.ix[:, 0]==chromosome]
            if str(chromosome) == "nan": continue
            for strand in ["+", "-"]:
                selected_bedgraph = bedgraph_chr[bedgraph_chr.ix[:, 4]==strand]
                if selected_bedgraph.shape[0] == 0: continue
                plt.axhline(0, linewidth=1.5, linestyle="-", color="k")

                if strand == "+":
                    if peak:
                        selected_peak = self.peak[self.peak["chrom"]==chromosome]
                        selected_peak = selected_peak[selected_peak.ix[:, 5]==strand]

                    ax = plt.subplot(len(chromosomes), 1, i_th+1)
                    ax.step(selected_bedgraph.ix[:, 1], selected_bedgraph.ix[:, 3], "ko-",
                            markersize=3, label="BedGraph", where='post', alpha=0.6, zorder=1)
                    if peak:
                        if not selected_peak.empty:
                            ax.scatter(selected_peak.ix[:, "peak_pos"].astype(int), selected_peak.ix[:, 2].astype(int),
                                    s=10, lw=0, c="r", zorder=10,
                                    #markerfmt=markerfmt, linefmt=linefmt,
                                    label="CalledPeaks")
                    ax.spines['right'].set_visible(False)
                    ax.spines['top'].set_visible(False)
                    ax.xaxis.set_ticks_position('bottom')
                    ax.yaxis.set_ticks_position('left')
                else:
                    if peak:
                        selected_peak = self.peak[self.peak["chrom"]==chromosome]
                        selected_peak = selected_peak[selected_peak.ix[:, 5]==strand]

                    ax = plt.subplot(len(chromosomes), 1, i_th+1)
                    ax.step(selected_bedgraph.ix[:, 1], -selected_bedgraph.ix[:, 3], "ko-",
                            markersize=3, label="BedGraph", where='post', alpha=0.6, zorder=1)
                    if peak:
                        if not selected_peak.empty:
                            ax.scatter(selected_peak.ix[:, "peak_pos"].astype(int), -selected_peak.ix[:, 2].astype(int),
                                       s=10, lw=0, c="r", zorder=10,
                                       #markerfmt=markerfmt, linefmt=linefmt,
                                       label="CalledPeaks")
                    ax.spines['right'].set_visible(False)
                    ax.spines['top'].set_visible(False)
                    ax.xaxis.set_ticks_position('bottom')
                    ax.yaxis.set_ticks_position('left')
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            ax.set_xlim(xstart, xend)
            ax.set_ylim(ystart, yend)
            ax.set_xlabel("position")
            ax.set_ylabel("height")
            if peak:
                ax.set_title("Peak Calling: {}".format(chromosome))
            else:
                ax.set_title("BedGraph: {}".format(chromosome))
            handles, labels = ax.get_legend_handles_labels()
            legend_unique = dict(zip(labels, handles))
            ax.legend(legend_unique.values(), legend_unique.keys(), fontsize=9)
        plt.tight_layout()

    def show(self, block=True):
        plt.show(block=block)


class ReportVisualizer(object):

    def __init__(self, fdr_report, from_file=False):
        self.report = fdr_report
        if not from_file:
            if not isinstance(self.report, ReportedPeak):       # pragma: no cover
                raise TypeError("inappropriate type: {}. expected {}".format(type(self.report), type(ReportedPeak())))
        if from_file:
            self.report = ReportedPeak.load(self.report)
            #self.significance_level = significance_level

    def draw(self, significance_level=0.05):
        self.significance_level = significance_level
        plt.figure(figsize=(9,6))
        n_peaks = self.report.shape[0]
        fdr_peak = self.report["FDR_<="]
        fdr_peak = sorted(fdr_peak, reverse=False)

        ax = plt.subplot(111)
        ax.plot(np.arange(n_peaks), fdr_peak, "ko-", linewidth=1, markersize=3)
        ax.axhline(significance_level, color="r")
        ax.annotate("P={}".format(significance_level), fontsize=10,
                    xy=(0.1, significance_level+0.01),
                    color="r", horizontalalignment="left")
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')
        ax.set_ylim(-0.005, max(fdr_peak)+0.005)
        ax.set_xlabel("peak")
        ax.set_ylabel("P-values")
        ax.set_title("P-values of Peaks")

    def draw_each(self, refseq):
        if type(refseq) == int:     # pragma: no cover
            if refseq >= self.report.shape[0]:
                raise IndexError("index should be lower than report shape: {}".format(self.report.shape[0]))
            fig = plt.figure(figsize=(9,6))
            one = self.report.iloc[refseq]
            threshold_peak = map(int, one[4].split(","))[:-1]
            cumulative_prob = map(float, one[5].split(","))[:-1]

            ax = plt.subplot(111)
            ax.plot(threshold_peak, cumulative_prob, "ko-", markersize=5)
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.xaxis.set_ticks_position('bottom')
            ax.yaxis.set_ticks_position('left')
            ax.set_ylim(-0.002, 1.005)
            ax.set_xlabel("peak height")
            ax.set_ylabel("P-value")
            ax.set_title(one[0])
        elif type(refseq) == str:       # pragma: no cover
            if refseq not in self.report["RefSeq"].values:      # pragma: no cover
                print " * `{}` is not in report. Try with other RefSeq".format(refseq)
            else:       # pragma: no cover
                fig = plt.figure(figsize=(9,6))
                one = self.report[self.report["RefSeq"]==refseq].values[0]
                threshold_peak = map(int, one[4].split(","))
                cumulative_prob = map(float, one[5].split(","))
                ax = plt.subplot(111)
                ax.plot(threshold_peak, cumulative_prob, "ko-", markersize=5)
                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)
                ax.xaxis.set_ticks_position('bottom')
                ax.yaxis.set_ticks_position('left')
                ax.set_ylim(-0.002, 1.005)
                ax.set_xlabel("peak height")
                ax.set_ylabel("P-value")
                ax.set_title(refseq)

    def show(self, block=True):
        plt.show(block=block)


class ClusterVisualizer(object):

    def __init__(self, smooth_bedgraph, called_peak, bedgraph_from_file=False, peak_from_file=False):
        self.bedgraph = smooth_bedgraph
        if not bedgraph_from_file:
            if not isinstance(self.bedgraph, SmoothBedGraph):  #type(self.bedgraph) != type(SmoothBedGraph()):      # pragma: no cover
                raise TypeError("inappropriate type: {}. expected {}".format(type(self.bedgraph), type(SmoothBedGraph())))
        if bedgraph_from_file:
            self.bedgraph = SmoothBedGraph.load(self.bedgraph)
            # self.bedgraph.columns = ["chrom", "chrstart", "chrend", "overlap", "strand"]
        self.peak = called_peak
        if not peak_from_file:
            if (not isinstance(self.peak, ReportedPeak)) and (not isinstance(self.peak, CalledPeak)):       # pragma: no cover
                raise TypeError("inappropriate type: {}. expected {} or {}".format(type(self.peak), type(ReportedPeak()), type(CalledPeak())))
            self.peak = self.peak.reset_index(drop=True)
        if peak_from_file:
            try:        # pragma: no cover
                self.peak = CalledPeak.load(called_peak)
                # self.peak.columns = ["RefSeq", "peak", "peak_height", "chrom", "peak_pos", "strand"]
            except ValueError:
                self.peak = ReportedPeak.load(called_peak)
                # self.peak.columns = ["RefSeq", "length", "expression", "exp_tag", "threshold",
                                     # "thres_cum_prob", "peak", "peak_height", "chrom", "peak_pos", "strand", "FDR_<="]

        self.cluster_relative = None
        self.significant_position = None
        self.significancy = None
        self.total_clusters = None
        self.footprint = None
        self.cluster_plot = None
        self.window_plot = None

    def plot_clusters(self, n_minimum_tags=30, fig=None, xstart=-250, xend=250,
                      alpha=.3, verbose=True):
        if self.cluster_relative is not None:
            clusters_rel = self.cluster_relative
        else:
            clusters = cluster_reads(self.bedgraph, verbose)
            clusters_rel = get_relative_position(clusters, self.peak, verbose)
            clusters_rel.sort_values(by="chrstart", inplace=True)
            clusters_rel.reset_index(inplace=True)
            self.clusters = clusters
            self.cluster_relative = clusters_rel
            self.total_clusters = clusters_rel.shape[0]

        if n_minimum_tags:
            clusters_rel = clusters_rel[clusters_rel.n_tags >= n_minimum_tags]
            self.cluster_relative = clusters_rel
            self.total_clusters = clusters_rel.shape[0]

        if fig is None:
            fig = plt.figure()
        ax = plt.subplot(111)
        xx = zip(clusters_rel.chrstart.values, clusters_rel.chrend.values)

        if self.total_clusters == 0:        # pragma: no cover
            raise NoClusterError("No clusters were defined")

        step_size = 1./self.total_clusters
        altitude = step_size
        progress = 0
        progress_total = clusters_rel.shape[0]

        [ax.plot(x, [float(i+1)/self.total_clusters]*2, color="#f96f57", alpha=alpha)\
         for i, x in enumerate(xx)]
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.xaxis.set_ticks_position('bottom')
        ax.yaxis.set_ticks_position('left')
        ax.set_xlim(xstart, xend)
        ax.set_ylim(0, 1.01)
        ax.set_xlabel("position from cluster peak")
        ax.set_ylabel("fraction")

        self.cluster_plot = (fig, ax)
        return fig, ax

    def count_significancy(self, n_minimum_tags=30, verbose=True):
        if self.cluster_relative is not None:
            clusters_rel = self.cluster_relative
        elif self.significancy is None:     # pragma: no cover
            clusters = cluster_reads(self.bedgraph, verbose)
            clusters_rel = get_relative_position(clusters, self.peak, verbose)
            clusters_rel.sort_values(by="chrstart", inplace=True)
            clusters_rel.reset_index(inplace=True)
            self.clusters = clusters
            self.total_clusters = clusters_rel.shape[0]
            self.cluster_relative = clusters_rel
        elif self.significancy[0] != n_minimum_tags:        # pragma: no cover
            clusters_rel = self.cluster_relative
        else:       # pragma: no cover
            clusters = cluster_reads(self.bedgraph, verbose)
            clusters_rel = get_relative_position(clusters, self.peak, verbose)
            clusters_rel.sort_values(by="chrstart", inplace=True)
            clusters_rel.reset_index(inplace=True)
            self.clusters = clusters
            self.total_clusters = clusters_rel.shape[0]
            self.cluster_relative = clusters_rel
        st_very = int(clusters_rel["chrstart"].min())
        en_very = int(clusters_rel["chrend"].max())
        template = np.zeros(en_very - st_very + 1)

        significancy = OrderedDict()
        total_clusters_above_siglevel = 0
        count_df = clusters_rel[clusters_rel.n_tags >= n_minimum_tags]
        for idx, row in tqdm(count_df.iterrows(), desc="COUNTING", total=count_df.shape[0]):
            st = int(row["chrstart"] - st_very)
            en = int(row["chrend"] - st_very + 1)
            template[st:en] += 1
            total_clusters_above_siglevel += 1
        template = template / float(total_clusters_above_siglevel)

        for idx, fraction in enumerate(template):
            significancy[idx+st_very] = fraction
        self.significancy = (n_minimum_tags, significancy)

        return significancy

    def plot_footprint(self, significance_level=0.95, n_minimum_tags=30,
                       xstart=-250, xend=250, alpha=.1, verbose=True):
        if self.significancy is None:       # pragma: no cover
            self.count_significancy(n_minimum_tags)
        elif self.significancy[0] != n_minimum_tags:        # pragma: no cover
            self.count_significancy(n_minimum_tags)
        fig, ax = self.plot_clusters(n_minimum_tags=n_minimum_tags, xstart=xstart,
                                     xend=xend, alpha=alpha, verbose=verbose)

        ax.plot(self.significancy[1].keys(), self.significancy[1].values(),
                linestyle="-", color="#ff5335", linewidth=2, alpha=1)

        significancy = pd.DataFrame(np.array(self.significancy[1].items()))
        significancy = significancy[significancy[1] >= significance_level]
        if significancy.empty:      # pragma: no cover
            raise NoClusterError("no clusters above {}% significancy".format(significance_level))
        sig_left = int(significancy[0].min())
        sig_right = int(significancy[0].max())
        self.significant_position = [sig_left, sig_right]

        ax.axvline(sig_left, color="k", linestyle="--", linewidth=1.5)
        ax.axvline(sig_right, color="k", linestyle="--", linewidth=1.5)
        ax.annotate("{}%".format(significance_level*100), xy=(sig_left-6, 0.97),
                    color="k", fontsize=18, horizontalalignment="right",
                    verticalalignment="top")
        ax.annotate("{}".format(sig_left), xy=(sig_left-6, 0.01), color="k", fontsize=12,
                    horizontalalignment="right")
        ax.annotate("+{}".format(sig_right), xy=(sig_right+6, 0.01), color="k", fontsize=12,
                    horizontalalignment="left")
        ax.set_xlim(xstart, xend)

        peak = self.peak.copy()
        self.footprint = pd.concat([peak["chrom"], peak["peak_pos"].astype(int)+sig_left,
                                    peak["peak_pos"].astype(int)+sig_right, peak["RefSeq"],
                                    peak["strand"]], axis=1)
        self.footprint.columns = ["chrom", "chrstart", "chrend", "RefSeq", "strand"]

        window_st = self.peak["peak_pos"].astype(int) + sig_left
        window_en = self.peak["peak_pos"].astype(int) + sig_right
        self.windowed_peak = pd.concat([self.peak["chrom"], window_st, window_en, self.peak["strand"],
                                        self.peak["peak_pos"].astype(str), self.peak["peak_height"].astype(float),
                                        self.peak["FDR_<="].astype(float),
                                        self.peak["RefSeq"], self.peak["expression"]], axis=1)
        self.windowed_peak.columns = ["chrom", "chrstart", "chrend", "strand",
                                      "position", "height", "p_val<=",
                                      "RefSeq", "expression"]
        self.windowed_peak.height.astype(int)

        # grouped = self.windowed_peak.groupby(["chrom", "chrstart", "chrend", "strand"])
        # refseqs = grouped["RefSeq"].apply(lambda x: "/".join(x)).reset_index()["RefSeq"]
        # expressions = grouped["expression"].apply(lambda x: "/".join(map(str, x))).reset_index()["expression"]
        #
        # self.windowed_peak.drop_duplicates(inplace=True)
        # self.windowed_peak.RefSeq = refseqs
        # self.windowed_peak.expression = expressions

        self.window_plot = (fig, ax)
        return fig, ax

    def show(self, block=True):
        plt.show(block=block)


def cluster_reads(bedgraph, verbose=True):
    smooth_1 = smoothing_bedgraph(bedgraph, 1, verbose, return_list=True)
    #smooth_1.to_csv("trouble.txt", sep="\t", header=None, index=False)
    spacer = pd.DataFrame([["nan"]*bedgraph.shape[1]], columns=bedgraph.columns).values
    smooth_1 = [i for i in smooth_1 if np.all(i.values != spacer)]
    col_names=["chrom", "chrstart", "chrend", "strand", "n_tags"]

    clusters = [[i.chrom.values[0], i.iloc[0].chrstart, i.iloc[-1].chrend,
                 i.strand.values[0], i.shape[0]] for i in smooth_1 if not i.empty]
    clusters = pd.DataFrame(clusters, columns=col_names)

    return clusters


def get_relative_position(clusters, called_peak, verbose=True):
    # use sqlite3
    # clusters: ["chrom", "chrstart", "chrend", "strand", "n_tags"]
    # called_peak: ["chrom", "chrstart", "chrend", "peak", "peak_height", "peak_pos", "strand"]
    con = db.connect(get_temp_data())
    clusters.to_sql("clusters", con, index=False, if_exists="replace")
    cp = called_peak.copy()
    try:        # pragma: no cover
        cp.columns = ["RefSeq", "length", "expression", "exp_tag", "threshold", "thres_cum_prob", "peak", "peak_height", "chrom2", "peak_pos", "strand2", "FDR_<="]
    except ValueError:      # pragma: no cover
        cp.columns = ["RefSeq", "peak", "peak_height", "chrom2", "peak_pos", "strand2"]
    cp.to_sql("called_peak", con, index=False, if_exists="replace")
    interval = pd.read_sql("""SELECT * FROM clusters AS c
                           INNER JOIN called_peak AS p ON c.chrom = p.chrom2
                           AND c.chrstart <= p.peak_pos AND c.chrend >= p.peak_pos
                           AND c.strand = p.strand2""", con)
    interval["chrstart"] = interval["chrstart"].astype(int) - interval["peak_pos"].astype(int)
    interval["chrend"] = interval["chrend"].astype(int) - interval["peak_pos"].astype(int)
    for col in ["strand", "RefSeq", "peak", "peak_height", "chrom2", "peak_pos", "strand2"]:
        del interval[col]

    con.execute("DROP TABLE clusters")
    con.execute("DROP TABLE called_peak")
    con.commit(); con.close(); del con
    clusters_rel = interval

    return clusters_rel
