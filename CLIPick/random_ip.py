# COPYRIGHT (C) 2016 By Park, Sihyung
# module for main `in silico Random IP` process

import sys, random, datetime
import numpy as np
import scipy as sp
import scipy.stats
import multiprocessing as mp
import pandas as pd
from utils import *

def _base_honest(args):
    """
    base model for in silico Random IP.
    """

    # turn chained assignment warning off
    pd.options.mode.chained_assignment = None

    processed_expression_profile, lower, upper, read_type, read_length, n_iter, fragsize = args
    res = processed_expression_profile.copy()

    try:        # pragma: no cover
        # if exp_tag != 0, then run in silico random IP.
        row_index = res.exp_tag!=0
        threshold = get_threshold_apply(res.loc[row_index], n_iter, lower, upper, read_type, read_length, fragsize)
        res.loc[row_index, "threshold"] = threshold.apply(lambda row: ",".join(map(str, zip(*row)[0]))).values
        res.loc[row_index, "thres_cum_prob"] = threshold.apply(lambda row: ",".join(map(str, (np.array(zip(*row)[1])/float(n_iter)).cumsum().round(3)))).values

        # if exp_tag == 0, then set threshold & thres_cum_prob to 0.
        row_index = res.exp_tag==0
        res.loc[row_index, "threshold"] = "0"
        res.loc[row_index, "thres_cum_prob"] = "0"
    except ValueError:      # pragma: no cover
        # if exp_tag == 0, then set threshold & thres_cum_prob to 0.
        row_index = res.exp_tag==0
        res.loc[row_index, "threshold"] = "0"
        res.loc[row_index, "thres_cum_prob"] = "0"

        # if exp_tag != 0, then run in silico random IP.
        row_index = res.exp_tag!=0
        threshold = get_threshold_apply(res.loc[row_index], n_iter, lower, upper, read_type, read_length, fragsize)
        res.loc[row_index, "threshold"] = threshold.apply(lambda row: ",".join(map(str, zip(*row)[0]))).values
        res.loc[row_index, "thres_cum_prob"] = threshold.apply(lambda row: ",".join(map(str, (np.array(zip(*row)[1])/float(n_iter)).cumsum().round(3)))).values

    res.fillna(0, inplace=True)

    return ISRIPTags(res)


def honest(processed_expression_profile, lower=20, upper=80,
           read_type="p", read_length=100, n_iter=500,
           random_state=False, write_file=False, n_jobs=None, frag_avg_size=50):
    """
    Run in silico Random IP.
    """

    # turn chained assignment warning off
    pd.options.mode.chained_assignment = None

    if not isinstance(processed_expression_profile, Processed):     # pragma: no cover
        raise TypeError("inappropriate type: {}. expected {}".format(type(processed_expression_profile), Processed))
    if random_state: random.seed(random_state)

    if n_jobs is None:
        if mp.cpu_count() >= 2:     # pragma: no cover
            n_jobs = mp.cpu_count() - 1
        else: n_jobs = 1        # pragma: no cover

    if n_jobs > 1:       # pragma: no cover
        pool = mp.Pool(processes=n_jobs)
        df_split = np.array_split(processed_expression_profile, n_jobs)
        args_list = list(zip(df_split, [lower]*n_jobs, [upper]*n_jobs, [read_type]*n_jobs, [read_length]*n_jobs, [n_iter]*n_jobs, [frag_avg_size]*n_jobs))
        pool_results = pool.map(_base_honest, args_list)
        res = pd.concat(pool_results)
    elif n_jobs == 1:
        #pool = mp.Pool(processes=n_jobs)
        #df_split = np.array_split(processed_expression_profile, n_jobs)
        #args_list = list(zip(df_split, [lower]*n_jobs, [upper]*n_jobs, [read_type]*n_jobs, [read_length]*n_jobs, [n_iter]*n_jobs, [frag_avg_size]*n_jobs))
        #pool_results = pool.map(_base_honest, args_list)
        res = _base_honest((processed_expression_profile, lower, upper, read_type, read_length, n_iter, frag_avg_size))
        #res = pd.concat(pool_results)
    else: raise ValueError("n_jobs should be an integer greater than or equal to 1")        # pragma: no cover

    if write_file:
        ts = str(datetime.datetime.now()).split()[0]
        res.to_csv("{}_isrip_honest.txt".format(ts), sep="\t", header=None, index=False)
        print " * written as: {}_isrip_honest.txt".format(ts)
    return ISRIPTags(res)


def _base_clever(processed_expression_profile, write_file=False, verbose=False):        # pragma: no cover
    """
    base model for `clever` approach.
    """
    df = processed_expression_profile
    if not isinstance(df, Processed):  # type(df) != type(Processed()):       # pragma: no cover
        raise TypeError("inappropriate type: {}. expected {}".format(type(df), type(Processed())))
    res = df.copy()

    for idx in range(df.shape[0]):
        length = res.ix[idx, "length"]
        exp_tag = res.ix[idx, "exp_tag"]
        if exp_tag == 0:
            res.ix[idx, "threshold"] = "0"
            res.ix[idx, "thres_cum_prob"] = "0"
            continue

        lam = (49.*exp_tag)/length
        thres = [sp.stats.poisson.ppf((i)**(1./exp_tag), lam, 2) for i in [0.999, 0.99, 0.98, 0.97, 0.96, 0.95, 0.94, 0.93, 0.92, 0.91, 0.9]]
        thres.append(0)

        res.ix[idx, "threshold"] = ",".join(map(str, map(int, thres)))
        res.ix[idx, "thres_cum_prob"] = ",".join(map(str, [0.001, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 1]))
        if verbose:
            if idx%50 == 0:     # pragma: no cover
                prog = round(float(idx) / df.shape[0] * 100., 1)
                sys.stdout.flush()
                sys.stdout.write("\r * processing: {}%".format(prog))

    if verbose:
        sys.stdout.flush()
        sys.stdout.write("\r * complete: 100.0%\n")
    res = res.fillna(0)
    if write_file:
        ts = str(datetime.datetime.now()).split()[0]
        res.to_csv("{}_isrip_clever.txt".format(ts), sep="\t", header=None, index=False)
        print " * written as: {}_isrip_clever.txt".format(ts)

    return ISRIPTags(res)


def _clever(processed_expression_profile, write_file=False, verbose=False, n_jobs=1):       # pragma: no cover
    """
    Run ISRIP with `clever` approach.
    """
    if n_jobs == 1:     # pragma: no cover
        return _base_clever(processed_expression_profile, write_file, verbose)
    elif n_jobs > 1:        # pragma: no cover
        raise ValueError("Multiprocessing is not supported yet")
        data = processed_expression_profile
        part = data.shape[0]//n_jobs
        devided_works = [data[part*i:part*(i+1)] for i in range(0, n_jobs-1)]
        devided_works.append(data[part*(n_jobs-1):])

        returns = map(partial(_base_clever, n_iter=n_iter, write_file=False,
                              verbose=verbose), devided_works)

        result = pd.concat(returns, axis=0)
        if write_file:
            result.to_csv("{}_isrip_clever.txt".format(ts), sep="\t", header=None, index=False)

        return ISRIPTags(result)
