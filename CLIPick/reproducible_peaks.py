import sys, re, sqlite3
import pandas as pd
import subprocess
import numpy as np
from preprocessing import *
from peak_calling import *
from datasets import *

def split_df_by_strand(bed):
# """
# input - one DataFrame object, header = True
# output - list of 2 Dataframe objects, one for '+' and one for '-' strand
# """
    splitted_beds = []
    col = ['chrom', 'chrstart', 'chrend', 'strand']
    dtypes = np.dtype([('chrom', str), ('chrstart', int), ('chrend', int), ('strand', str)])
    data = np.empty(0, dtype=dtypes)
    splitted_beds.append(pd.DataFrame(data))
    splitted_beds.append(pd.DataFrame(data))
    for index, row in bed.iterrows():
        if row['strand'] == '+':
            splitted_beds[0].loc[len(splitted_beds[0])] = row
        else:
            splitted_beds[1].loc[len(splitted_beds[1])] = row
    return splitted_beds

def get_reproducible_peaks(files, gen, threshold=2):
# """
# input: files - list of BED files
# genome - reference genome file
# threshold - integer, default = 1 (peak is reproduced at least once)
# """
    genome = get_genome_file(gen)
    sorted_files = []
    merged_files = ''
    merged = []

    for i in files:
        sorted_files.append(i + '.sorted')
        to_cmd_sort = 'sort -k1,1 -k2,2n -k3,3n ' + i + ' > ' + i + '.sorted'
        subprocess.Popen(to_cmd_sort, shell = True).wait()

    for i in sorted_files:
        to_cmd = 'bedtools merge -c 4,6 -o distinct,distinct -s -i ' + i + '> ' + i + '.merged'
        subprocess.Popen(to_cmd, shell = True).wait()
        merged_files += i + '.merged '
        merged.append(i + '.merged ')

    to_cmd = 'cat ' + merged_files + ' > temp_merged.bed'
    p = subprocess.Popen(to_cmd, shell = True)
    p.wait()

    to_cmd = 'sort -k1,1 -k2,2n -k3,3n temp_merged.bed | bedtools merge -c 4,6 -o distinct,distinct -s -i - > clusters.bed'
    p = subprocess.Popen(to_cmd, shell = True)
    p.wait()

    to_cmd_sort = 'sort -k1,1 -k2,2n -k3,3n temp_merged.bed > temp_merged.bed.sorted'
    p = subprocess.Popen(to_cmd_sort, shell = True)
    p.wait()
    os.remove('temp_merged.bed')

    to_cmd = 'bedtools genomecov -bg -max ' + threshold + ' -i temp_merged.bed.sorted -g ' + genome + ' -strand + > temp_bedgraph+.bed'
    to_cmd1 = 'bedtools genomecov -bg -max ' + threshold + ' -strand - -i temp_merged.bed.sorted -g ' + genome + ' > temp_bedgraph-.bed'
    p = subprocess.Popen(to_cmd, shell = True)
    p.wait()

    p = subprocess.Popen(to_cmd1, shell = True)
    p.wait()

    bed0 = pd.DataFrame()
    bed1 = pd.DataFrame()

    if os.path.getsize('./temp_bedgraph+.bed'):
        bed0 = pd.read_csv('temp_bedgraph+.bed', sep="\t", header = None)
    if os.path.getsize('./temp_bedgraph-.bed'):
        bed1 = pd.read_csv('temp_bedgraph-.bed', sep="\t", header = None)

    if not (bed0.empty):
        bed0[4] = bed0[list(bed0)[-1]]
        bed0[5] = '+'

    if not bed1.empty:
        bed1[4] = bed1[list(bed1)[-1]]
        bed1[5] = '-'

    if not (bed0.empty & bed1.empty):
        bed = [bed0, bed1]
        bg = pd.concat(bed)
    else:
        if not bed0.empty:
            bg = bed0
        else:
            bg = bed1

    os.remove('temp_bedgraph-.bed')
    os.remove('temp_bedgraph+.bed')

    bg = bg[bg[3] >= int(threshold)]
    bg = bg.sort_values(by = [0, 1, 2])
    bg.index = range(len(bg))

    bg.to_csv('temp_to_intersect.bed', sep = '\t', index = False, header = None)

    to_cmd = 'bedtools intersect -sorted -u -s -a clusters.bed -b temp_to_intersect.bed > to_intersect.bed'
    p = subprocess.Popen(to_cmd, shell = True)
    p.wait()

    to_cmd1 = ''
    for i in sorted_files:
        to_cmd = 'bedtools intersect -sorted -u -s -a ' + i + ' -b to_intersect.bed > ' + i + '.overlaped'
        p = subprocess.Popen(to_cmd, shell = True)
        p.wait()
        to_cmd1 += i + '.overlaped '

    to_cmd_last = 'cat ' + to_cmd1 + ' > temp_res.bed'
    p = subprocess.Popen(to_cmd_last, shell = True)
    p.wait()

    rm = ''
    for i in sorted_files:
         rm = i + '.overlaped'
         os.remove(rm)

    col = ['chrom', 'chrstart', 'chrend', 'name', 'score', 'strand']
    res = pd.read_csv('temp_res.bed', names = col, sep="\t")
    res = res.sort_values(by = ['chrom','chrstart','chrend'])

    os.remove('to_intersect.bed')
    os.remove('temp_res.bed')
    os.remove('temp_to_intersect.bed')
    os.remove('temp_merged.bed.sorted')
    os.remove('clusters.bed')

    for i in sorted_files:
        merged = i + '.merged'
        os.remove(i)
        os.remove(merged)
    return res

def mock_expression(genome):
    gen = get_refseq_data()
    conn = sqlite3.connect(gen)
    cursor = conn.cursor()
    st = 'SELECT name FROM ' + genome
    cursor.execute(st)
    names = cursor.fetchall()
    name = []
    for i in names:
        h = []
        h = list(i)
        if h[0] not in name:
            name.append(h[0])
    ones = []
    zeros = []
    ln = len(name)- 1
    for i in name:
        ones.append(1)
        zeros.append(0.0)
    conn.close()
    res = ISRIPTags({'RefSeq' : name, 'length' : ones, 'expression' : ones, 'exp_tag' : ones, 'threshold' : '0,0', 'thres_cum_prob' : '0,0'})
    return res
