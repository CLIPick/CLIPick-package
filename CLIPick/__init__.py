# ISRIP - in silico Random IP
""" This module allows easy four-step analysis of each sequenced peaks
from IP-seq-based experiments.

COPYRIGHT (c) 2016 by Sihyung Park, Sung Wook Chi. All Rights Reserved.
"""

#import isrip.datasets, isrip.graphics, isrip.peak_calling, isrip.preprocessing,
#       isrip.random_ip, isrip.report, isrip.utils
import os

_ROOT = os.path.abspath(os.path.dirname(__file__))
def get_data(path):
    return os.path.join(_ROOT, 'sample_data', path)

def get_genome_file(assembly):
    return os.path.join(_ROOT, 'refseq_data', assembly+".genome")

def get_refseq_data():
    return os.path.join(_ROOT, 'refseq_data', 'genome.db')

def get_exome_data():
    return os.path.join(_ROOT, 'refseq_data', 'exome.db')

def get_temp_data():
    return os.path.join(_ROOT, 'refseq_data', 'temp.db')
