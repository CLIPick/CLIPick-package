# COPYRIGHT (C) 2016 By Park, Sihyung
# module for ISRIP report - annotate FDR to each CLIP peaks.

import pandas as pd
import numpy as np
import datetime, os
from utils import ReportedPeak, ISRIPTags, CalledPeak, IPTags

def fdr_report(isrip, clip, isrip_from_file=False, clip_from_file=False, write_file=False):
    """
    * Input
    isrip: DataFrame object returned from `random_ip.honest()`.
        If you want to get isrip result from written file, set `from_file=True`.
    clip: DataFrame object returned from `load_custom_ip()` or `PeakCaller().call()`.
    """
    if isrip_from_file:
        try:        # pragma: no cover
            isrip = ISRIPTags.load(isrip)
            # isrip.columns = ["RefSeq", "length", "expression", "exp_tag", "threshold", "thres_cum_prob"]
        except ValueError:      # pragma: no cover
            raise IOError("input should be a bed file of ISRIP result")
    if clip_from_file:
        try:        # pragma: no cover
            clip = CalledPeak.load(clip)
            # clip.columns = ["RefSeq", "peak", "peak_height", "chrom", "peak_pos", "strand"]
        except ValueError:      # pragma: no cover
            raise IOError("input should be a bed file of peaks called by `PeakCaller()` object")
    if not isinstance(isrip, ISRIPTags):        # pragma: no cover
        raise TypeError("inappropriate type: {}. expected {}".format(type(isrip), type(ISRIPTags())))
    if (not isinstance(clip, CalledPeak)) and (not isinstance(clip, IPTags)):       # pragma: no cover
        raise TypeError("inappropriate type: {}. expected {} or {}".format(type(clip), type(CalledPeak()), type(IPTags())))

    df = pd.merge(isrip, clip, on="RefSeq", how="inner")
    df = df.reset_index(drop=True)
    if df.empty:    # pragma: no cover
        return ReportedPeak(df)

    df.ix[:, "FDR_<="] = df.apply(calculate_fdr, axis=1).values

    if write_file:
        ts = str(datetime.datetime.now()).split()[0]; n=2
        fname = "{}_FDR_report.txt".format(ts)
        if os.path.exists(fname):
            while os.path.exists("{}_FDR_report({}).txt".format(ts, n)):
                n += 1
            fname = "{}_FDR_report({}).txt".format(ts, n)
            df.to_csv(fname, sep="\t", header=None, index=False)
        else:
            df.to_csv("{}_FDR_report.txt".format(ts), sep="\t", header=None, index=False)
        print " * written as: {}".format(fname)
    return ReportedPeak(df)

def calculate_fdr(row):
    thres = np.array(map(int, row["threshold"].split(",")))
    prob = np.array(map(float, row["thres_cum_prob"].split(",")))
    peak_height = float(row["peak_height"])
    idx = peak_height >= thres
    return prob[idx][0]
