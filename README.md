# CLIPick: Package for analysis of HITS-CLIP and variants
![ISRIP_logo](./docs/ISR-CLIP_logo.png)

**CLIPick** is a Package for bioinformatic analysis of high-throughput sequencing coupled with cross-linking followed by immunoprecipitation (HITS-CLIP or CLIP-seq) reads.

Copyright (c) 2016 by Sihyung Park, Sung Wook Chi.

- Docs can be found here: https://naturale0.github.io/CLIPick/


&ensp;

# Key features
(optional) Find CLIP tags reproduced in several replicates
1. HITS-CLIP Peak calling
2. *in silico* Random IP: peak significancy test
3. RNAbp footprint analysis: window size determination
4. Visualization: [samples](https://naturale0.github.io/CLIPick/graphics/#gallery)


&ensp;

# Quick start

## (optional) Find reproduceable peaks
*This option is not avaliable in CLIPick-GUI*

In case there are 2 or more replicates avaliable, CLIPick can find CLIP tags with specified biological complexity.
Files used in this example:

replicate_1.bed
```
chr1    100065  100115  replicate1_1 1       +
chr1    100100  100150  replicate1_2 1       +
chr1    100170  100220  replicate1_3 1       +
chr1    100270  100320  replicate1_4 1       -
chr1    100330  100380  replicate1_5 1       +
chr1    100490  100540  replicate1_6 1       +
```

replicate_2.bed
```
chr1    100000  100050  replicate2_1 2       +
chr1    100060  100110  replicate2_2 2       +
chr1    100200  100250  replicate2_3 2       -
chr1    100260  100310  replicate2_4 2       -
chr1    100381  100431  replicate2_5 2       +
chr1    100441  100491  replicate2_6 2       +
```

Finding CLIP tags with biological complexity 2:
```python
>>> import CLIPick.reproducible_peaks.py
>>>
>>> # list of names of replicates files
>>> list_of_files = ["replicate_1.bed", "replicate_2.bed"]
>>>
>>> # integer for biological complexity
>>> biological_complexity = 2
>>>
>>> BC2 = get_reproducible_peaks(list_of_files, "mm9", biological_complexity)
```
Now `BC2` is a dataframe containing all CLIP tags that belong to clusters of biological complexity 2

```
chr1    100060  100110  replicate2_2 2   +
chr1    100065  100115  replicate1_1 1   +
chr1    100100  100150  replicate1_2 1   +
chr1    100260  100310  replicate2_4 2   -
chr1    100270  100320  replicate1_4 1   -
chr1    100441  100491  replicate2_6 2   +
chr1    100490  100540  replicate1_6 1   +
```

## 1. Peak calling
Peak calling from BED file of [p13 mouse neocortex AGO HITS-CLIP](http://www.nature.com/nature/journal/v460/n7254/abs/nature08170.html). Robust Ago CLIP reads, which were derived reproducibly from experiments using two different Ago antibodies (biological complexity (BC) ≥ 2), were [retrieved](http://ago.korea.ac.kr/Ago_Clip_data). CLIPick only uses unique reads from input BED file.

### 1) Convert BED to BEDGraph
BED file used in this example can be downloaded in [here](http://clip.korea.ac.kr/CLIPick/Tags_OL_7G1-1_2A8_RefSeq.bed). You can also load the example data by following lines in python.

```python
>>> from CLIPick.datasets import load_p13clip
>>> from CLIPick.peak_calling import BedGraphConverter, PeakCaller
>>>
>>> # Loads reloaded P13 mouse neocortex AGO HITS-CLIP data.
>>> # Original file is located under /CLIPick/sample_data/Tags_OL_7G1-1_2A8_RefSeq.bed
>>> bed = load_p13clip().data
>>>
>>> # convert BED to BEDGraph
>>> bgc = BedGraphConverter()
>>> bgc.fit(bed, "mm8", from_dataframe=True)
>>> bedgraph = bgc.transform()
```
```
 * Warning: `transform` process might take a huge amount of time
```

Now `bedgraph` is a dataframe of converted BEDGraph, with spacers(nan) inserted between each of the gap. ([download](http://clip.korea.ac.kr/CLIPick/Tags_OL_7G1-1_2A8_RefSeq.bedgraph))

```
chrom	chrstart	chrend	overlap	strand
chr1	4827083	4827115	1	+
nan	nan	nan	nan	nan
chr1	4831148	4831154	1	+
chr1	4831154	4831155	2	+
chr1	4831155	4831157	3	+
…
``` 


### 2) Peak calling
```python
>>> # Determines peak positions
>>> pc = PeakCaller()
>>> pc.fit(bedgraph, "mm8")
>>> called_peaks = pc.call()
```

`called_peaks` is a dataframe of determined peak positions. ([download](http://clip.korea.ac.kr/CLIPick/called_peaks_BEFORE.txt))

```
RefSeq	peak	peak_height	chrom	peak_pos	strand
NM_008866	Peak	1	chr1	4827083	+
NM_008866	Peak	6.0	chr1	4831182	+
NM_008866	Peak	8.0	chr1	4835078	+
NM_008866	Peak	9.0	chr1	4835092	+
NM_008866	Peak	10.0	chr1	4835098	+
…
```

* Visualization

```python
>>> # show peak positions in genomic scale
>>> from CLIPick.graphics import BedGraphVisualizer
>>>
>>> bv = BedGraphVisualizer(bedgraph, called_peaks)
>>> bv.draw(peak=True, xstart=2.091e7+9680, xend=2.093e7-7480)
>>> bv.show()
```

![peak_call](./docs/gaussed_peaks.png)


## 2. *in silico* Random IP

### (optional) Skip *in silico* Random IP
*This option is not avaliable in CLIPick-GUI*

In case when expression profile is not avaliable, *in silico* Random IP step can be skipped by using `mock_expression`
```python
>>> import CLIPick.reproducible_peaks.py
>>>
>>> mock_isrip_res = mock_expression("mm9")
>>>
```
`mock_isrip_res` is a dataframe imitating in silico random IP result.

```
RefSeq         length   expression    exp_tag   threshold         thres_cum_prob
NM_016868      1        1             1         0,0              0,0
NM_016869      1        1             1         0,0              0,0
NM_001033128   1        1             1         0,0              0,0 
NM_016862      1        1             1         0,0              0,0  
NM_016863      1        1             1         0,0              0,0
```

### 1) load and preprocess expression profile
Calculate FDR(false discovery rate) of each peaks.

```python
>>> # Loads P13 mouse neocortex expression profile
>>> from CLIPick.datasets import load_p13array
>>>
>>> array = load_p13array().data
```

`array` is a dataframe of P13 mouse neocortex expression profile.

```
RefSeq         length   expression
NM_016868      6947     98.929900
NM_016869      4863     42.452550
NM_001033128   5597     1535.636007
NM_016862      4599     1173.803113
NM_016863      980      973.192817
```

```python
>>> # calculate expected tag number, for in silico random IP
>>> from CLIPick.preprocessing import ExpTagCalculator
>>> 
>>> cal = ExpTagCalculator()
>>> array = cal.fit_transform(array, dtype="array", total_tags=bgc.total_reads)
```

Then, exp_tag column is appended to the `array` dataframe.

```
RefSeq         length   expression    exp_tag
NM_016868      6947     98.929900     4
NM_016869      4863     42.452550     1
NM_001033128   5597     1535.636007   50
NM_016862      4599     1173.803113   31
NM_016863      980      973.192817    6
```

### 2) process random IP

```python
>>> # use `honest` method to simulate background noise
>>> from CLIPick.random_ip import honest
>>> 
>>> isrip_res = honest(array, n_iter=1000)
```

`isrip_res` is a dataframe of *in silico* random IP result. ([download](http://clip.korea.ac.kr/CLIPick/random_IP.txt))

```
RefSeq         length   expression    exp_tag   threshold         thres_cum_prob
NM_016868      6947     98.929900     6         4,3,2,1,0         0.0,0.001,0.202,1.0,1.0  
NM_016869      4863     42.452550     2         3,2,1,0           0.0,0.033,1.0,1.0 
NM_001033128   5597     1535.636007   81        8,7,6,5,4,3,0     0.0,0.003,0.033,0.281,0.901,1.0,1.0  
NM_016862      4599     1173.803113   51        8,7,6,5,4,3,2,0   0.0,0.002,0.007,0.09,0.554,0.992,1.0,1.0  
NM_016863      980      973.192817    9         6,5,4,3,2,1,0     0.0,0.004,0.052,0.401,0.99,1.0,1.0 
```

Probability of having peak height greater than or equal to the `threshold` height is calculated in `thres_cum_prob`, respectively. 

### 3) estimate p-value of each CLIP-seq peak

```python
>>> from CLIPick.report import fdr_report
>>> 
>>> result = fdr_report(isrip_res, clip, write_file=False)
```

`result` is a dataframe of CLIP-seq peaks and their corresponding p-values. ([download](http://clip.korea.ac.kr/CLIPick/called_peaks_BEFORE_with_expression.txt))

```
RefSeq         length   expression    exp_tag   threshold         thres_cum_prob                             peak   peak_height   chrom   peak_pos   strand   FDR_<=  
NM_016868      6947     98.929900     6         4,3,2,1,0         0.0,0.001,0.202,1.0,1.0                    Peak   4.0           chr7    16190546   -        0.00  
NM_016869      4863     42.452550     2         3,2,1,0           0.0,0.033,1.0,1.0                          Peak   12.0          chr19   55679377   +        0.00  
NM_001033128   5597     1535.636007   81        8,7,6,5,4,3,0     0.0,0.003,0.033,0.281,0.901,1.0,1.0        Peak   2.0           chr12   4855625    -        0.99  
NM_016862      4599     1173.803113   51        8,7,6,5,4,3,2,0   0.0,0.002,0.007,0.09,0.554,0.992,1.0,1.0   Peak   2.0           chr12   4855656    -        0.99  
NM_016863      980      973.192817    9         6,5,4,3,2,1,0     0.0,0.004,0.052,0.401,0.99,1.0,1.0         Peak   2.0           chr12   4855689    -        0.99  
```

* Visualize calculated p-values to get overall insight into the data

```python
>>> from CLIPick.graphics import ReportVisualizer
>>> rv = ReportVisualizer(result)
>>> rv.draw()
>>> rv.show()
```

![isrip_report](./docs/isrip_report.png)

Filter out peaks with p-value higher than 0.05. ([download](http://clip.korea.ac.kr/CLIPick/called_peaks_AFTER_with_expression.txt))

```python
>>> result = result.filter_out(0.05)
```


## 3. Window size (footprint) estimation

Finds 95% interval of read clusters.

Under `ClusterVisualizer().plot_clusters()`, every CLIP-seq tags are merged into clusters. Then each cluster is positioned relative to the adjacent peak position. Window size is determined by the frequency of existing clusters in each positions. ’95% window size’ refers to 'window size which contains at least 95% of all clusters'.

```python
>>> from CLIPick.graphics import ClusterVisualizer
>>>
>>> cv = ClusterVisualizer(bedgraph, result)
>>> cv.plot_clusters(n_minimum_tags=30)  # ignore clusters with less than 30 supporting tags.
>>> cv.show()
```

![cluster](./docs/cluster_position_15.png)

```python
>>> cv.plot_footprint(significance_level=0.95, n_minimum_tags=30)
>>> cv.show()
```

![Ago_footprint](./docs/footprint_15.png)

After window size estimation, we can get the final output (windowed peaks) as below. ([download](http://clip.korea.ac.kr/CLIPick/CLIPick_OUTPUT.txt))

```python
>>> cv.windowed_peak.to_csv("CLIPick_windowed_peaks.bed", sep="\t", index=False, header=False)  # save final output file as BED format. 
```

`cv.windowed_peak` is a dataframe of BED format, with each row representing one called peak with estimated width.

```
chrom  chrstart    chrend     RefSeq:name:position  height strand   p_val<=
chr7   16190514  16190572  NM_016868:Peak:16190546     4.0      -         0
chr19  55679345  55679403  NM_016862:Peak:55679377    12.0      +         0
chr17  70329709  70329767  NM_177639:Peak:70329741     8.0      +     0.001
chr17  70329746  70329804  NM_177639:Peak:70329778     8.0      +     0.001
chr17  70562345  70562403  NM_177639:Peak:70562377     8.0      +         0
```


&ensp;

# Installation

In UNIX terminal, follow commands below.

```
$ git clone https://gitlab.com/chi_lab/CLIPick-package
$ cd CLIPick
$ python setup.py install
$ cd ..
$ rm -rf CLIPick

$ python
>>> import CLIPick
```

You need to have [NumPy](http://www.numpy.org), [SciPy](http://scipy.org), [pandas](http://pandas.pydata.org), [matplotlib](http://matplotlib.org) and [BedTools](http://bedtools.readthedocs.io/en/latest/) installed on your computer for all `CLIPick` functions to work properly.

**You need to install BedTools manually!** Please download and install BedTools following [the guides](http://bedtools.readthedocs.io/en/latest/content/installation.html).


&ensp;

# Documentation

For full docs, please visit [here](https://naturale0.github.io/CLIPick/).

&ensp;

# Dependency
`CLIPick` heavily depends on the packages listed below:

* Python packages
 - [NumPy](http://www.numpy.org) >= 1.11.1
 - [SciPy](http://scipy.org) >= 0.18.1
 - [Pandas](http://pandas.pydata.org) >= 0.18.1, <= 0.22.0
 - [matplotlib](http://matplotlib.org) >= 1.5.1, <= 2.2.2
 - [Cython](http://cython.org) >= 0.25.1 (optional)
* others (**NEED MANUAL INSTALL**)
 - [BedTools](http://bedtools.readthedocs.io/en/latest/) >= 2.25.0

&ensp;

# References
- Random IP - modified and enhanced from *in silico* Random CLIP:  
Chi, S. W., Zang, J. B., Mele, A., & Darnell, R. B. (2009). Argonaute HITS-CLIP decodes microRNA–mRNA interaction maps. Nature. doi:10.1038/nature08170
- bedtools 2 (for BED-to-BEDGraph conversion only):  
Quinlan AR and Hall IM, 2010. BEDTools: a flexible suite of utilities for comparing genomic features. Bioinformatics. 26, 6, pp. 841–842.
- RNA-Seq Atlas:  
RNA-Seq Atlas - A reference database for gene expression profiling in normal tissue by next generation sequencing. Bioinformatics (Oxford, England), 10.1093/bioinformatics/bts084 (Krupp et al.).
- P13 mouse neocortex exon-array:  
Chi, S. W., Zang, J. B., Mele, A., & Darnell, R. B. (2009). Argonaute HITS-CLIP decodes microRNA–mRNA interaction maps. Nature. doi:10.1038/nature08170
- human cardiac tissue AGO2 HITS-CLIP:  
Spengler RM, Zhang X, Cheng C, McLendon JM et al. Elucidation of transcriptome-wide microRNA binding sites in human cardiac tissues by Ago2 HITS-CLIP. Nucleic Acids Res 2016 Sep 6;44(15):7120-31. PMID: 27418678
- Genome reference assemblies were retrieved from [UCSC genome browser](http://genome.ucsc.edu/):  
Kent WJ, Sugnet CW, Furey TS, Roskin KM, Pringle TH, Zahler AM, Haussler D. The human genome browser at UCSC. Genome Res. 2002 Jun;12(6):996-1006.
- Human RefSeq genes (hg16, hg17, hg18, hg19, hg38):  
The Genome Sequencing Consortium. Initial sequencing and analysis of the human genome. Nature. 2001 Feb 15;409(6822):860-921.
- Mouse RefSeq genes (mm7, mm8, mm9):  
Mouse Genome Sequencing Consortium. Initial sequencing and comparative analysis of the mouse genome. Nature. 2002 Dec 5;420(6915):520-62. PMID: 12466850
- Mouse RefSeq genes (mm10) is provided by the [Genome Reference Consortium (GRC)](https://www.ncbi.nlm.nih.gov/grc)([acknowledgements](https://www.ncbi.nlm.nih.gov/grc/mouse)), and retrieved from UCSC genome browser.
