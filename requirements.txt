numpy == 1.11.1
scipy == 1.1.0
pandas <= 0.22.0
matplotlib >= 1.5.1, <= 2.2.2
Cython >= 0.25.1
tqdm >= 4.23.0
