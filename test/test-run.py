import matplotlib as mpl
mpl.use("Agg")
from CLIPick.peak_calling import *
from CLIPick.graphics import *
from CLIPick.datasets import *
from CLIPick.preprocessing import *
from CLIPick.random_ip import honest, _base_honest
from CLIPick.report import *
from CLIPick.utils import *
from Tkinter import TclError
import pandas as pd

def test_CLIPick():
    ## -------------- PEAK_CALL -------------- ##
    bgc = BedGraphConverter()
    #bgc.fit("https://gitlab.com/chi_lab/CLIPick-package/raw/master/CLIPick/sample_data/test.bed", "mm8")
    #bedgraph = bgc.transform()
    bedgraph = bgc.fit_transform("https://gitlab.com/CLIPick/CLIPick-package/raw/master/CLIPick/sample_data/test.bed",
                                 "mm8",
                                 #read_length=35, avg_frag_size=50
                                 )

    df_bed = pd.read_csv("https://gitlab.com/CLIPick/CLIPick-package/raw/master/CLIPick/sample_data/test.bed", sep="\t", header=None)
    bgc2 = BedGraphConverter()
    bgc2.fit_transform(df_bed, "mm8", from_dataframe=True, drop_st_en=False, write_file=True,
                       #read_length=35, avg_frag_size=50
                       )

    pc = PeakCaller()
    pc.fit(bedgraph, "mm8")
    called_peaks = pc.call()

    bedgraph.to_csv("test.bedgraph", sep="\t", index=False, header=None)
    pc2 = PeakCaller()
    pc2.fit("test.bedgraph", "mm8", from_file=True)
    pc2.call(write_file=True)

    ## -------------- DATASETS -------------- ##
    array_o = load_custom_array("https://gitlab.com/CLIPick/CLIPick-package/raw/master/CLIPick/sample_data/test.array", "mm8")
    load_p13array().data
    load_ago2clip().data
    load_rnaseq().data
    load_p13clip().data
    load_genome("mm8")
    load_exome("mm8")
    rnaseq_o = load_custom_rnaseq("https://gitlab.com/CLIPick/CLIPick-package/raw/master/CLIPick/sample_data/test.array", "mm8")
    load_custom_ip("https://gitlab.com/CLIPick/CLIPick-package/raw/master/CLIPick/sample_data/test.ip", [0, 1, 2], header=None)
    load_custom_genome("https://gitlab.com/CLIPick/CLIPick-package/raw/master/CLIPick/sample_data/test.genome.bed")
    load_custom_exome("https://gitlab.com/CLIPick/CLIPick-package/raw/master/CLIPick/sample_data/test.exome.bed")



    ## -------------- PREPROCESSING -------------- ##
    calc = ExpTagCalculator()
    array = calc.fit_transform(array_o, dtype="array", total_tags=bgc.total_reads)
    calc.fit(array_o, dtype="array")
    calc.transform(total_tags=bgc.total_reads)
    calc.fit_transform(rnaseq_o, dtype="rnaseq", total_tags=bgc.total_reads)

    ## -------------- RANDOM_IP -------------- ##
    isrip = honest(array, n_iter=2)
    honest(array, n_iter=2, write_file=True)
    isrip_base_h = _base_honest((array, 20, 80, "p", 100, 2, 50))

    ## -------------- REPORT -------------- ##
    fdr_peaks = fdr_report(isrip, called_peaks)
    fdr_report(isrip, called_peaks, write_file=True)
    filtered_peaks = fdr_peaks.filter_out(1.)
    calculate_fdr(pd.merge(isrip, called_peaks, on="RefSeq", how="inner").iloc[0])

    ## -------------- GRAPHICS -------------- ##
    try:
        bv = BedGraphVisualizer(bedgraph, filtered_peaks)
        bv.draw(peak=True)
        bv.draw()
        bv.show(block=False)
    except NoClusterError, TclError: pass
    try:
        rd = ReportVisualizer(fdr_peaks)
        rd.draw()
        rd.show(block=False)
    except NoClusterError, TclError: pass
    try:
        cv = ClusterVisualizer(bedgraph, filtered_peaks)
        try:
            cv.plot_clusters(n_minimum_tags=1)
            cv.show(block=False)
        except NoClusterError, TclError: pass
        try:
            cv.count_significancy(n_minimum_tags=1)
        except NoClusterError, TclError: pass
        try:
            cv.plot_footprint(significance_level=0.5, n_minimum_tags=1)
            cv.show(block=False)
        except NoClusterError, TclError: pass
    except NoClusterError, TclError: pass

    return

if __name__ == "__main__":
    test_CLIPick()
